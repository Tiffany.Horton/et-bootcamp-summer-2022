import React, { useMemo } from 'react';
import { BankBetter } from './BankBetter';
import { Features } from './Features';
import { Navigation } from './Navigation';
import { Footer } from './Footer'
import { Header } from './Header';

export const PageSections = ({ content = {} }) => {
  const bankBetterContent = useMemo(() => content.pocPageSections && content.pocPageSections.find((section) => section.contentType === 'pocBankBetter'), [content]);
  const featuresContent = useMemo(() => content.pocPageSections && content.pocPageSections.find((section) => section.contentType === 'pocFeaturesSection'), [content]);
  const footerContent = useMemo(() => content.pocPageSections && content.pocPageSections.find((section) => section.contentType === 'pocFooter'), [content]);
  const headerContent = useMemo(() => content.pocPageSections && content.pocPageSections.find((section) => section.contentType === 'pocHeader'), [content]);
  return (
    <div>
      {!!headerContent && <Header content={headerContent} />}

      {content.pocPageSections && !!content.pocPageSections.length &&
        <Navigation sections={content.pocPageSections} />
      }
      {!!featuresContent && <Features content={featuresContent} />}
      {!!bankBetterContent && <BankBetter content={bankBetterContent} />}
      {!!footerContent && <Footer content={footerContent} />}
    </div>
  )
}
