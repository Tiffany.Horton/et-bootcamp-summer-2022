import { useEffect, useState } from 'react';
import { fetchContentById } from '../services/content';
import { CONTENTFUL_PAGE_ENTITY_ID, CONTENTFUL_SITE_ENTITY_ID } from '../constants';
import { log } from '../utils';
import { TextHeading, TextBody, Box } from '@ally/metronome-ui';
import { Section } from '../components/Section';

export const CDA = () => {
  const [content, setContent] = useState({ fields: {} });
  const [siteContent, setSiteContent] = useState({ fields: {} });
  useEffect(() => {
    (async () => {
      setContent(await fetchContentById(CONTENTFUL_PAGE_ENTITY_ID));
      setSiteContent(await fetchContentById(CONTENTFUL_SITE_ENTITY_ID));
    })();
  }, []);
  const { fields } = content;
  const { title, subtitle, text, sections } = fields;
  log.debug('my site content', siteContent);
  return (
    <div>
      <TextHeading tag="h1" size="xl" color="plum" style={{ textAlign: 'center' }}>{title}</TextHeading>
      <TextHeading tag="h2" size="md" style={{ textAlign: 'center' }}>{subtitle}</TextHeading>
      <p tag="p" style={{ textAlign: 'center' }}>{text}</p>
      <div style={{ paddingTop: '48px' }}>
        <Box display="flex" flexDirection="row" flexWrap="wrap" justifyContent="center">
          {sections && sections.map(({ fields = {} }) => {

            return (
              <Section fields={fields} />
            )
          })}
        </Box>
      </div>
    </div>
  );
}
